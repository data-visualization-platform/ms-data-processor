### 1)

``` sudo apt-get install g++ git libboost-atomic-dev libboost-thread-dev libboost-system-dev libboost-date-time-dev libboost-regex-dev libboost-filesystem-dev libboost-random-dev libboost-chrono-dev libboost-serialization-dev libwebsocketpp-dev openssl libssl-dev ninja-build ```

### 2)

``` g++ -std=c++11 main.cpp -o main -lboost_system -lcrypto -lssl -lcpprest ```
``` ./main ```

### 3)

open results.html