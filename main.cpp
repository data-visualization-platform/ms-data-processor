#include <cpprest/http_listener.h>
#include <cpprest/json.h>
#pragma comment(lib, "cpprest110_1_1")

#include <iostream>
#include <map>
#include <set>
#include <string>

using namespace std;
using namespace web;
using namespace web::http;
using namespace web::http::experimental::listener;

#define TRACE(msg) wcout << msg

map<utility::string_t, utility::string_t> dictionary;


void handle_get(http_request request)
{
    TRACE(U("\nhandle GET\n"));

    json::value obj;
    for (auto const &p : dictionary)
    {
        obj[p.first] = json::value::string(p.second);
    }

    request.reply(status_codes::OK, obj);
}


int main()
{
    http_listener listener(U("http://localhost:8090/api"));

    listener.support(methods::GET, handle_get);

    try
    {
        listener
        .open()
        .then([&listener]() {TRACE(U("\nStarting to listen on port 8090\n"));})
        .wait();

        while (true);
    }
    catch (exception const & e)
    {
        wcout << e.what() << endl;
    }

    return 0;
}